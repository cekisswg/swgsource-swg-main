package script.systems.veteran_reward;

import script.menu_info;
import script.menu_info_types;
import script.obj_id;
import script.string_id;

public class instant_travel_terminal_deed extends script.base_script
{
    public instant_travel_terminal_deed()
    {
    }
    public static final string_id LEARN_ABILITY = new string_id("item_n", "instant_travel_terminal_learn");
    public int OnObjectMenuRequest(obj_id self, obj_id player, menu_info mi) throws InterruptedException
    {
        mi.addRootMenu(menu_info_types.ITEM_USE, LEARN_ABILITY);
        return SCRIPT_CONTINUE;
    }
    public int OnObjectMenuSelect(obj_id self, obj_id player, int item) throws InterruptedException
    {
        if (item == menu_info_types.ITEM_USE)
        {
            String lvl = getConfigSetting("GameServer","itv_level");
            if(lvl == null || lvl.length() == 0) lvl = "20";
            if(getLevel(player) < Integer.parseInt(lvl)){
                sendSystemMessage(player, "Notice: Instant Travel vehicles may not be used until you have reached level " + lvl + ".", null);
            }
            if (hasObjVar(self, "privateerShip"))
            {
                grantCommand(player, "callforprivateerpickup");
            	destroyObject(self);
            }
            else if (hasObjVar(self, "royalShip"))
            {
                grantCommand(player, "callforroyalpickup");
            	destroyObject(self);
            }
            else if (hasObjVar(self, "junk"))
            {
                grantCommand(player, "callforrattletrappickup");
            	destroyObject(self);
            }
            else if (hasObjVar(self, "tcg_itv_home"))
            {
                grantCommand(player, "callforsolarsailerpickup");
            	destroyObject(self);
            }
            else if (hasObjVar(self, "tcg_itv_location"))
            {
                grantCommand(player, "callforg9riggerpickup");
            	destroyObject(self);
            }
            else 
            {
                grantCommand(player, "callforpickup");
            	destroyObject(self);
            }
        }
        return SCRIPT_CONTINUE;
    }
}
